[![Support me on Patreon](https://i.imgur.com/FzTLsYV.png) ](https://www.patreon.com/kkthnx)[![Donate through Paypal](https://i.imgur.com/IkPCLeh.png)](https://www.paypal.me/kkthnx) [![Say Thanks](https://img.shields.io/badge/Say%20Thanks-!-1EAEDB.svg)](https://saythanks.io/to/Kkthnx)

This will mainly mute the annoying Lament of the Highborne toy + some other annoying sounds.

## How to install
If not, and you need to do this manually, then download and unzip it to your `World of Warcraft` directory. This does NOT go into Interface or AddOns folders.
Also, make sure the game is closed when moving new files into the addon directory as it is unable to discover new files when it's already running.

## Suggestions

* [Suggestions](https://github.com/kkthnx-wow/Annoying_WoW_Sounds/issues/new)

## Join the community
There are thousands of users, but most are content to simply download and use the interface without further ado. If you wish to get more involved though, have some questions you can't find answers to anywhere else or simply just wish to stop by and say hello, we have both a [discord](https://discordapp.com/) server and a facebook page.

* [Discord](https://discord.gg/YUmxqQm)
* [Facebook](https://www.facebook.com/kkthnxui)
* [Twitter](https://twitter.com/KkthnxUI)

## Buy me a coffee!
Donations are welcome, but not required to use the UI at all. Donations help me further my development and fuel my gaming! Donations will never be a requirement to use the UI! If you would like to donate, you can do so down below.

* [PayPal](https://www.paypal.me/kkthnx)
* [Patreon](https://www.patreon.com/kkthnx)
* [Steam Wishlist](https://www.curseforge.com/linkout?remoteUrl=http%253a%252f%252fstore.steampowered.com%252fwishlist%252fid%252fKkthnx)

Regards
Josh *"Kkthnx"* Russell

![unknown](https://user-images.githubusercontent.com/40672673/59269316-c1ce3380-8c1c-11e9-8fce-ebe3e564c3b3.png)

